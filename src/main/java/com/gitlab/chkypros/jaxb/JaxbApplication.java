package com.gitlab.chkypros.jaxb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class JaxbApplication {

    public static void main(String[] args) {
        SpringApplication.run(JaxbApplication.class, args);
    }

    @PostMapping(value = "/greet")
    public String greet(@RequestBody Person person) {
        return person.getAge() > 27
            ? getFormalGreeting(person)
            : getInformalGreeting(person);
    }

    private String getFormalGreeting(Person person) {
        return String.format("Hello %s %s", person.getFirstName(), person.getLastName());
    }

    private String getInformalGreeting(Person person) {
        return String.format("Hey %s!", person.getFirstName());
    }
}
